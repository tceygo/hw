//https://api.github.com/repos/javascript-tutorial/en.javascript.info/commits

const data = fetch("https://api.github.com/repos/javascript-tutorial/en.javascript.info/commits");
data.then((info)=> {
    return info.json();
}).then((info)=> {
    console.log(info);
    info.forEach((element)=> {
        const p = document.createElement("p");
        p.innerHTML = element.commit.author.name;
        document.body.append(p);

        const d = document.createElement("p");
        d.innerText = element.commit.author.date;
        document.body.append(d);

        const m = document.createElement("p");
        m.innerHTML = element.commit.message;
        document.body.append(m);
    })
})