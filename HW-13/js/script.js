window.addEventListener('DOMContentLoaded', () => {
  const data = new XMLHttpRequest();
  data.open(
    'GET',
    'https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json'
  );
  data.send();
  data.onreadystatechange = () => {
    if (data.readyState === 4 && data.status >= 200 && data.status < 300) {
      const dataResponse = JSON.parse(data.responseText);
      dataResponse.forEach( element => {
        const currency = document.createElement("td");
        const rate = document.createElement("td");
        const tr = document.createElement("tr");
        currency.innerText = element.txt;
        rate.innerText = element.rate.toFixed(2);
        document.querySelector("tbody").append(tr);
        tr.append(currency, rate);



          
      });
    }
  };
});
