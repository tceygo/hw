//Реализовать функцию для создания объекта "пользователь".
//Написать функцию createNewUser(), которая будет создавать 
//и возвращать объект newUser. При вызове функция должна спросить
 //у вызывающего имя и фамилию. Используя данные, введенные пользователем, 
 //создать объект newUser со свойствами firstName и lastName. Добавить в объект
 // newUser метод getLogin(), который будет возвращать первую букву имени пользователя,
 //  соединенную с фамилией пользователя, все в нижнем регистре (например, Ivan Kravchenko → ikravchenko).
 //   Создать пользователя с помощью функции createNewUser(). Вызвать у пользователя функцию getLogin().
 //    Вывести в консоль результат выполнения функции.




//  Возьмите выполненное задание выше (созданная вами функция createNewUser()) и дополните ее следующим 
//функционалом:

//  При вызове функция должна спросить у вызывающего дату рождения (текст в формате dd.mm.yyyy) и 
//сохранить ее в поле birthday.
//  Создать метод getAge() который будет возвращать сколько пользователю лет.
//  Создать метод getPassword(), который будет возвращать первую букву имени пользователя в 
//верхнем регистре, 
//  соединенную с фамилией (в нижнем регистре) и годом рождения. (например, Ivan Kravchenko 
//13.03.1992 → Ikravchenko1992).
 
 
//  Вывести в консоль результат работы функции createNewUser(), а также функций getAge() 
//и getPassword() созданного объекта.
 
 
                    
 

  class CreateNewUser {
      constructor(name, lname, date) {
          this.firstName = name;
          this.lastName = lname;
          this.birthday = date;
      }

      getLogin (){
          return this.firstName[0].toLowerCase() + this.lastName.toLowerCase(); 
      }
      getAge (){
          return parseInt(new Date().getFullYear()) - parseInt(this.birthday.slice(6));
         // console.dir(new Date().getFullYear());
         // document.write( new Date().getFullYear());
      }
      getPassword(){
          return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(6);
      }
  }  

  const newUser = new CreateNewUser(prompt("Введите Ваше имя", "Mikola"), prompt("Введите Вашу фамилию", "Kolupaka"), prompt("Ведите дату рождения в формате: dd.mm.yyyy", "12.08.2001"));
  
  console.log(newUser.getLogin());
  console.log(newUser.getAge());
  console.log(newUser.getPassword());




                       
// Написать функцию filterBy(), которая будет принимать в себя 2 аргумента. 
// Первый аргумент - массив, который будет содержать в себе любые данные, второй аргумент - тип данных.
// Функция должна вернуть новый массив, который будет содержать в себе все данные, которые 
// были переданы в аргумент, за исключением тех, тип которых был передан вторым аргументом. 
// То есть, если передать массив ['hello', 'world', 23, '23', null], и вторым аргументом 
// передать 'string', то функция вернет массив [23, null].


function filterBy(arr, typ){
    return arr.filter(element => typeof element !== typ);
    //console.log(a);
    //return a;
}
console.log(filterBy(['hello', 'world', 23, '23', null, [3, null, "s"]], "object"));
document.write(filterBy(['hello', 'world', 23, '23', null], "number"));


                   