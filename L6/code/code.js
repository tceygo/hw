function Human(name, age){
    this.name = name;
    this.age = age;



}
Human.prototype.say = function (){
    document.write(`моє і'мя ${this.name} <br> мій вік ${this.age} <br>`);
}

const humans = [
    new Human(prompt('Введіть ваше імя: ', "Bill"), prompt('Введіть ваш вік: ', "18")),
    new Human(prompt('Введіть ваше імя: ', "Bill"), prompt('Введіть ваш вік: ', "18")), 
    new Human(prompt('Введіть ваше імя: ', "Bill"), prompt('Введіть ваш вік: ', "18")), 
    new Human(prompt('Введіть ваше імя: ', "Bill"), prompt('Введіть ваш вік: ', "18")),
];



// for (let i = 0; i < humans.length; i++) {
//     humans[i].say();
// }


// Функция изменяет входящий массив coll
//const bubbleSort = (humans) => {
    let stepsCount = humans.length - 1;
    // Объявляем переменную swapped, значение которой показывает был ли
    // совершен обмен элементов во время перебора массива
    let swapped;
    // do..while цикл. Работает почти идентично while
    // Разница в проверке. Тут она делается не до выполнения тела, а после.
    // Такой цикл полезен там, где надо выполнить тело хотя бы раз в любом случае.
    do {
      swapped = false;
      // Перебираем массив и меняем местами элементы, если предыдущий
      // больше, чем следующий
      for (let i = 0; i < stepsCount; i += 1) {
        if (humans[i].age > humans[i + 1].age) {
          // temp – временная константа для хранения текущего элемента
          const temp = humans[i];
          humans[i] = humans[i + 1];
          humans[i + 1] = temp;
          // Если сработал if и была совершена перестановка,
          // присваиваем swapped значение true
          swapped = true;
        }
      }
      // Уменьшаем счетчик на 1, т.к. самый большой элемент уже находится
      // в конце массива
      stepsCount -= 1;
    } while (swapped); // продолжаем, пока swapped === true
  
    //return humans;
  //};

  document.write("<hr>")
  for (i = 0; i < humans.length; i++) {
    humans[i].say();
}