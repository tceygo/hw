const div1 = document.querySelector(".first");
const div2 = document.querySelector(".sekond");

console.log(`зовніщній відступ ${getStyle(div1, "margin")}`);
console.dir(`зовнішній відступ ${div2.style.margin}`);


function getStyle (element, property){
    if (element.currentStyle){
        return element.currentStyle[property];
    } else if (window.getComputedStyle){
        return window.getComputedStyle(element)[property];
    }
}